var restify = require('restify');
 
const server = restify.createServer({
  name: 'myapp',
  version: '1.0.0'
});
 
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());

server.post('/tes',(req, res, next)=>{
    const data = (req.body.nilai);

    const datas = data.map((newdata)=>{
        return Number(newdata.nilai);
    })

    let avg = datas.reduce((a,b)=>{
        return (a+b)}) / data.length;

    object = {
        "nama " :req.body.nama,
        "kelas " :req.body.kelas,
        "Rata-rata" : avg 
    }

    res.send(object);
    return next();

})

 
server.listen(8080, function () {
  console.log('%s listening at %s', server.name, server.url);
});