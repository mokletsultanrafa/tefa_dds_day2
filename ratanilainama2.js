var restify = require('restify');
 
const server = restify.createServer({
  name: 'myapp',
  version: '1.0.0'
});
 
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());

server.post('/tes', function (req, res, next){

    semuadata = [];

    req.body.map((semua) => {

        var datas = semua.nilai.map((tes)=>{
            return Number(tes.nilai)
        });
       
        var avg = datas.reduce((holder,currentValue)=>{
            return (holder+currentValue)
        }) / semua.nilai.length;

        semuadata.push({
            "Nama " : semua.nama,
            "Kelas " : semua.kelas,
            "avgNilai " : avg
        });
    })
    res.send(semuadata);
    return next();
});

 
server.listen(8080, function () {
  console.log('%s listening at %s', server.name, server.url);
});